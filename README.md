# divider_with_cache

Designed internal structure of a fully associative cache using Content Addressable Memory (CAM) design without comparators, which includes 8x16 CAM, 8x16 RAM and LRU stack to supplement the LRU write back policy of the cache.